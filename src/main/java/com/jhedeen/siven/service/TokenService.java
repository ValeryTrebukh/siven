package com.jhedeen.siven.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class TokenService {

    private static Map<String, String> companies = new HashMap<>();
    private static Map<String, String> tokens = new HashMap<>();

    static {
        companies.put("UPS", "12047");
        companies.put("DHL", "12048");
        companies.put("FedEx", "12049");

        tokens.put("12047", UUID.randomUUID().toString());
        tokens.put("12048", UUID.randomUUID().toString());
        tokens.put("12049", UUID.randomUUID().toString());
    }

    public String getTokenId(String company) {
        return companies.get(company);
    }

    public String getToken(String id) {
        return tokens.get(id);
    }
}

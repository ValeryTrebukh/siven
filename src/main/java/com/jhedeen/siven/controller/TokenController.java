package com.jhedeen.siven.controller;

import com.jhedeen.siven.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @GetMapping(value = "/{company}")
    public String getTokenId(@PathVariable String company) {
        return tokenService.getTokenId(company);
    }


    @GetMapping(value = "/token/{tokenId}")
    public String getToken(@PathVariable String tokenId) {
        return tokenService.getToken(tokenId);
    }
}
